#Change this if you need to target a specific CMake version
cmake_minimum_required(VERSION 2.6)
project(minesweeper)

# Disable debug symbols by default
if(CMAKE_BUILD_TYPE STREQUAL "")
  set(CMAKE_BUILD_TYPE Release)
endif()
# (you can also set it on the command line: -D CMAKE_BUILD_TYPE=Release)

# Set version information in a config.h file
#set(minesweeper_VERSION_MAJOR 1)
#set(minesweeper_VERSION_MINOR 0)
#configure_file(
#  "${PROJECT_SOURCE_DIR}/config.h.in"
#  "${PROJECT_BINARY_DIR}/config.h"
#  )
include_directories("${PROJECT_BINARY_DIR}") 
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include/")


# Define sources and executable
set(CMAKE_CXX_FLAGS "-std=c++11")
set(EXECUTABLE_NAME "minesweeper")
set(MY_SOURCES 
   src/main.cpp
   src/texturemanager.cpp
   src/board.cpp
   src/tile.cpp
   src/state.cpp
   )
add_executable(${EXECUTABLE_NAME} ${MY_SOURCES})


# Detect and add SFML
# set(SFML_ROOT "d:/code/SFML-2.1")
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
#Find any version 2.X of SFML
#See the FindSFML.cmake file for additional details and instructions
#find_package(SFML 2 REQUIRED system window graphics network audio)
find_package(SFML 2 REQUIRED system window graphics)

if(SFML_FOUND)
  include_directories(${SFML_INCLUDE_DIR})
  target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
endif()


# Install target
install(TARGETS ${EXECUTABLE_NAME} DESTINATION bin)


# CPack packaging
#include(InstallRequiredSystemLibraries)
#set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
#set(CPACK_PACKAGE_VERSION_MAJOR "${minesweeper_VERSION_MAJOR}")
#set(CPACK_PACKAGE_VERSION_MINOR "${minesweeper_VERSION_MINOR}")
#include(CPack)
