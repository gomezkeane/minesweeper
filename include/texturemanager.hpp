#ifndef TEXTUREMANAGER_INCLUDED
#define TEXTUREMANAGER_INCLUDED

#include "main.hpp"
#include <string>
#include <unordered_map>

class TextureManager
{
public:
    TextureManager(const std::string& filename, unsigned width, unsigned height);
    virtual ~TextureManager();

    const sf::Texture& getTexture() const;
    const sf::IntRect& getTile(const std::string& tileName) const;
    unsigned width() const;
    unsigned height() const;
private:
    sf::Texture spriteSheet_m;
    unsigned width_m;
    unsigned height_m;
    std::unordered_map<std::string, sf::IntRect> spriteMap_m;

    void addTile(const std::string& name, int offset);
    void loadTexture(const std::string& name);

};


#endif // TEXTUREMANAGER_INCLUDED
