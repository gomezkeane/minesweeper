#ifndef STATE_HPP
#define STATE_HPP

#include "main.hpp"
#include "board.hpp"

enum class TileStates;

class State
{
    public:
        virtual void update(Board* board) = 0;
};

class InitBoard: public State
{
    public:
        InitBoard();
        virtual ~InitBoard();
        virtual void update(Board* board);
};

class Test: public State
{
    public:
        Test();
        virtual ~Test();
        virtual void update(Board* board);
};

#endif //STATE_HPP
