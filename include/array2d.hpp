#ifndef ARRAY2D_INCLUDED
#define ARRAY2D_INCLUDED

#include <memory>

template <typename T>
class Array2D
{
public:
    Array2D(unsigned width, unsigned height) : width_m(width), height_m(height)
    {
        if(width && height > 0)
        {
            array_m.reset(new T[width * height]);
        }
    }

    ~Array2D()
    {

    }


    T& operator() (unsigned x, unsigned y)
    {
        if(x < width_m && y < height_m)
        {
            return array_m[y * width_m + x];
        }
        else throw this;
    }

    const T& operator() (unsigned x, unsigned y) const
    {
        if(x < width_m && y < height_m)
        {
            return array_m[y * width_m + x];
        }
        else throw this;
    }

    unsigned getWidth() const
    {
        return width_m;
    }

    unsigned getHeight() const
    {
        return height_m;
    }

    unsigned getSize() const
    {
        return width_m * height_m;
    }

private:

    //Non Copiable and non Assignable
    Array2D(const Array2D<T>&);
    Array2D& operator = (const Array2D<T>&);

    unsigned width_m;
    unsigned height_m;
    std::unique_ptr<T[]> array_m;
};


#endif // ARRAY2D_INCLUDED
