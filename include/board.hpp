#ifndef BOARD_INCLUDED
#define BOARD_INCLUDED

#include <memory>
#include "tile.hpp"
#include "array2d.hpp"
#include "texturemanager.hpp"
class State;

class Board
{
public:
    Board(sf::RenderWindow& window, unsigned cols, unsigned rows);
    virtual ~Board();

    void update();
    Tile& operator()(unsigned x, unsigned y);
    const Tile& operator()(unsigned x, unsigned y) const;
    void displayBoard() const;

    unsigned rows();
    unsigned cols();
    std::unique_ptr<TextureManager> texture_m;
    std::unique_ptr<State> boardState_m;
    sf::RenderWindow& window_m;

private:
    //Non Copiable and non Assignable
    Board(const Board&);
    Board& operator= (const Board&);

    unsigned cols_m;
    unsigned rows_m;
    Array2D<Tile> tiles_m;
};

#endif // BOARD_INCLUDED
