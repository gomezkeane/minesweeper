#ifndef TILE_INCLUDED
#define TILE_INCLUDED

#include"main.hpp"

enum class TileStates
{
    unCovered,
    covered,
    flagged
};


class Tile
{
public:
    Tile();
    virtual ~Tile();

    bool isMine;
    int adjacentMines;
    TileStates state;
    sf::Sprite sprite;
};

#endif // TILE_INCLUDED
