#include <iostream>
#include <string>
#include <memory>

#include "main.hpp"
#include "texturemanager.hpp"
#include "array2d.hpp"
#include "board.hpp"
#include "state.hpp"


int main()
{
	sf::String TITLE = "Minesweeper";
	sf::Vector2i SCREENSIZE(800, 600);
	sf::VideoMode VIDEOMODE = sf::VideoMode(SCREENSIZE.x, SCREENSIZE.y);


	sf::RenderWindow window(VIDEOMODE, TITLE);
	window.setFramerateLimit(60);

	Board board(window, 7, 7);

	while (window.isOpen())
	{
		sf::Event event;

		while(window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) 
                window.close();
            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                    window.close();
		}

		//game.Update();
		//game.Render(window);
        board.update(); 
		window.clear();
        board.displayBoard();
       // window.draw(sprite);
		window.display();
	}



	return 0;
}
