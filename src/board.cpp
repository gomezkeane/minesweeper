#include "state.hpp"
#include "board.hpp"

Board::Board(sf::RenderWindow& window, unsigned cols, unsigned rows) : window_m(window), cols_m(cols), rows_m(rows), tiles_m(cols, rows)
{

    texture_m.reset(new TextureManager("tilestrip.png", 32, 32));
    boardState_m.reset(new InitBoard());
    update();
    boardState_m.reset(new Test());
}

Board::~Board()
{

}

void Board::update()
{
    if(boardState_m)
        boardState_m->update(this); 
}

Tile& Board::operator()(unsigned x, unsigned y)
{

   return tiles_m(x, y);
}

const Tile& Board::operator()(unsigned x, unsigned y) const
{

    return tiles_m(x, y);
}

void Board::displayBoard() const
{
    for(unsigned y = 0;y < rows_m; y++)
    {
        for(unsigned x = 0; x < cols_m; x++)
        {
            window_m.draw(tiles_m(x, y).sprite);
        }
    }
}

unsigned Board::rows()
{
    return rows_m;
}

unsigned Board::cols()
{
    return cols_m;
}

