#include "texturemanager.hpp"

TextureManager::TextureManager(const std::string& filename, unsigned width, unsigned height) : width_m(width), height_m(height)
{

    loadTexture(filename);

    int offset = 0;
    addTile("uncovered", offset);
    addTile("covered", offset+= width_m);
    addTile("1", offset+= width_m);
    addTile("2", offset+= width_m);
    addTile("3", offset+= width_m);
    addTile("4", offset+= width_m);
    addTile("5", offset+= width_m);
    addTile("6", offset+= width_m);
    addTile("7", offset+= width_m);
    addTile("8", offset+= width_m);
    addTile("mine", offset+= width_m);
    addTile("flag", offset+= width_m);
    addTile("badFlag", offset+= width_m);

}

TextureManager::~TextureManager()
{

}

const sf::Texture& TextureManager::getTexture() const
{
    return spriteSheet_m;
}

const sf::IntRect& TextureManager::getTile(const std::string& tileName) const
{
    return spriteMap_m.at(tileName);
}

void TextureManager::addTile(const std::string& name, int offset)
{
    sf::IntRect rect(offset, 0, width_m, height_m);
    spriteMap_m.emplace(name, rect);
}

void TextureManager::loadTexture(const std::string& name)
{
    sf::Image image;
    image.loadFromFile(name);
    image.createMaskFromColor(sf::Color::White);
    spriteSheet_m.loadFromImage(image);
}
        
unsigned TextureManager::width() const
{
    return width_m;
}

unsigned TextureManager::height() const
{
    return height_m;
}
