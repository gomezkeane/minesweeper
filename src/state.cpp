#include "state.hpp"

InitBoard::InitBoard()
{

}

InitBoard::~InitBoard()
{

}

void InitBoard::update(Board* board)
{

    for(unsigned y = 0;y < board->rows(); y++)
    {
        for(unsigned x = 0; x < board->cols(); x++)
        {
            (*board)(x, y).sprite.setTexture(board->texture_m->getTexture());
            (*board)(x, y).sprite.setTextureRect(board->texture_m->getTile("covered"));
            (*board)(x, y).sprite.setPosition(x * board->texture_m->width() , y * board->texture_m->height());
        }
    }
}

Test::Test()
{

}

Test::~Test()
{

}

void Test::update(Board* board)
{
    sf::Vector2i mouse = sf::Mouse::getPosition(board->window_m);
    sf::FloatRect AABB;
    TileStates tile = TileStates::covered;
    bool covered = false;

    for(unsigned y = 0; y < board->rows(); y++)
    {
       for(unsigned x = 0; x < board->cols(); x++)
       { 
           AABB = (*board)(x, y).sprite.getGlobalBounds();
           covered = false;
           if (tile == (*board)(x, y).state)
               covered = true;
           if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && covered && AABB.contains(static_cast<sf::Vector2f> (mouse)))
           {
                 (*board)(x, y).sprite.setTextureRect(board->texture_m->getTile("flag"));
                 (*board)(x, y).state = TileStates::flagged;
           }
         else if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && covered && AABB.contains(static_cast<sf::Vector2f>(mouse)))
         {
                 (*board)(x, y).sprite.setTextureRect(board->texture_m->getTile("uncovered"));
                 (*board)(x, y).state = TileStates::unCovered;
         }
       }
    }
}  



